# A model predictive control approach to optimally devise a two shot vaccination rollout
The code used in the paper "A model predictive control approach to optimally devise a two shot vaccination rollout".  
This standalone package contains the code and the dataset sufficient to reproduce all the quantitative results in the manuscript.

# Instructions
The code for the simulation is written in `PYTHON (Version: 3.7.7)` and can be used through ipython notebooks  
No further installations are required
The package has been tested on `MacOS 10.15 `and `Ubuntu 18.04.4 LTS`
All the dataset needed to reproduce the resutls are in `/data/`. 

# Organization
The `libMPC.py` file contains the epidemic progression as described in the manuscript and the MPC setup.  

The notebooks are organized as follow:
- 0_identification.ipynb: Code used to calibrate the model with the Italian COVID-19 outbreak 
- 1_MPC.ipynb: Contains the MPC and trivial strategies experiments. 




# Licence
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type"> A model predictive control approach to optimally devise a two shot vaccination rollout 2021 </span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
