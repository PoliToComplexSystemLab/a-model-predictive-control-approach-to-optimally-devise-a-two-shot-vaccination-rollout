import pandas as pd
import numpy as np
import scipy as spy
import os
import copy
import pickle
import itertools

from glob import glob
from functools import lru_cache

import matplotlib.ticker as ticker
import seaborn as sns
import matplotlib.patches as mpatches
from collections import Counter, defaultdict
from itertools import combinations

from tqdm import tqdm_notebook

import collections, functools, operator 
from bidict import bidict
from multiprocessing import Pool

import sys
from casadi import *
sys.path.append('../../../')
import do_mpc
import pickle
from tqdm import tqdm






class parameters():
    '''
    parameter calss containing the experiments details
    '''
    def __init__(self, par, mod=dict()):
        self.initComplete = False
        
        #Default parameters
        self.HMax = 37383.23081416955
        self.N = 59_394_207

        self.timeSimulation = 52  # number of weeks to simulate
        self.supplyNumber = 1_000_000 # number of Vaccine in a weekly supply
        
        #default vaccine parameters 
        self.alphaVacc=1/5
        self.sigmaVacc=0.5
        

        #Set additional user parameter
        for key, value in par.items():
            setattr(self, key, value)
        
        #Override default parameters using mod
        for key, value in mod.items():
            assert key in vars(self), "Invalid parameter: " + key
            setattr(self, key, value)

            
        #fixed parameters
        self.WH = self.HMax;
        self.WBeta = self.betaH - self.betaL;
        self.supplyWeekLim = self.supplyNumber*5
        
        self.initComplete = True
        
        
    def __setattr__(self,att,value):
        #do not allows parameters modification after init
        if "initComplete" in self.__dict__.keys():
            if self.initComplete:
                raise
        self.__dict__[att] = value
        
        

        
        

####################
# DEFINE MPC MODEL  
####################

def getModel(par, Dw="2w"):
    #Reads
    betaH, betaL, K, thHH = par.betaH, par.betaL, par.K, par.thH
    alpha, lambd, gamma, gamma2, I0_par, S0_par, H0_par, D0_par = 1/par.alpha, 1/par.lambd, 1/par.gamma, 1/par.gamma2, par.I0, par.S0, par.H0, par.D0

    supplyNumber, timeSimulation, supplyNumber = par.supplyNumber, par.timeSimulation, par.supplyNumber 
    alphaVacc,sigmaVacc,N = par.alphaVacc ,par.sigmaVacc ,par.N 
    

    #create model
    model = do_mpc.model.Model('discrete')
    
    #Variables
    S = model.set_variable('_x',  'S')
    I = model.set_variable('_x',  'I')
    R = model.set_variable('_x',  'R')
    H = model.set_variable('_x',  'H')
    D = model.set_variable('_x',  'D')
    beta = model.set_variable('_x',  'beta')
    F1 = model.set_variable('_x', 'F1')
    F2 = model.set_variable('_x', 'F2')
    F3 = model.set_variable('_x', 'F3')
    W = model.set_variable('_x',  'W')
    V = model.set_variable('_x',  'V')
    rema = model.set_variable('_x',  'rema')

    u1 = model.set_variable('_u',  'u1')
    u2 = model.set_variable('_u',  'u2')

    
    
    #Models ODEs using F1 and F2
    if Dw=="2w":
        model.set_rhs('S', 
                      S - beta*I*S/N + alphaVacc*W - u1 )
        model.set_rhs('I', 
                      I - gamma*I - alpha*I + beta*I/N*(S + F1 + F2 + (1-sigmaVacc)*W)  )
        model.set_rhs('R', 
                      R + gamma*I + gamma2*H)
        model.set_rhs('H', 
                      H + alpha*I - gamma2*H - lambd*H)
        model.set_rhs('D', 
                      D + lambd*H)  
        model.set_rhs('beta', 
                      betaL + (betaH-betaL) * (1+tanh(K*(thHH-H)))/2 )
        model.set_rhs('F1', 
                      u1)
        model.set_rhs('F2', 
                      F1*(1-beta*I/N) )
        model.set_rhs('F3', 
                      F2*0 )
        model.set_rhs('W', 
                      W  -  alphaVacc*W  +  F2*(1-beta*I/N) - (1-sigmaVacc)*beta*W*I/N - u2)
        model.set_rhs('V', 
                      V + u2)
        model.set_rhs('rema', 
                      rema + supplyNumber - u1 - u2)
        
    #Models ODEs using F1, F2 and F3
    if Dw=="3w":
        model.set_rhs('S', 
                      S - beta*I*S/N + alphaVacc*W - u1 )
        model.set_rhs('I', 
                      I - gamma*I - alpha*I + beta*I/N*(S + F1 + F2 + F3 + (1-sigmaVacc)*W)  )
        model.set_rhs('R', 
                      R + gamma*I + gamma2*H)
        model.set_rhs('H', 
                      H + alpha*I - gamma2*H - lambd*H)
        model.set_rhs('D', 
                      D + lambd*H)  
        model.set_rhs('beta', 
                      betaL + (betaH-betaL) * (1+tanh(K*(thHH-H)))/2 )
        model.set_rhs('F1', 
                      u1)
        model.set_rhs('F2', 
                      F1*(1-beta*I/N) )
        model.set_rhs('F3', 
                      F2*(1-beta*I/N) )
        model.set_rhs('W', 
                      W  -  alphaVacc*W  +  F3*(1-beta*I/N) - (1-sigmaVacc)*beta*W*I/N - u2)
        model.set_rhs('V', 
                      V + u2)
        model.set_rhs('rema', 
                      rema + supplyNumber - u1 - u2)
        
        
    model.setup()
    return model




#############################
# DEFINE MPC SIMULATOR  
#############################

def getSimulator(model, par):
    simulator = do_mpc.simulator.Simulator(model)
    params_simulator = {'t_step': 1}
    simulator.set_param(**params_simulator)
    
    simulator.setup()
    return simulator



#############################
# DEFINE MPC ESTIMATOR  
#############################

def getEstimator(model):
    estimator = do_mpc.estimator.StateFeedback(model)
    return estimator



#############################
# GET X0
#############################

@lru_cache(maxsize=32)
def getX0(par):
    '''
    Get array of initial conditions, using the condition specified in par
    '''

    #Reads
    betaH, betaL, K, thHH = par.betaH, par.betaL, par.K, par.thH
    alpha, lambd, gamma, gamma2, I0_par, S0_par, H0_par, D0_par = 1/par.alpha, 1/par.lambd, 1/par.gamma, 1/par.gamma2, par.I0, par.S0, par.H0, par.D0

    supplyNumber, timeSimulation, supplyNumber = par.supplyNumber, par.timeSimulation, par.supplyNumber 
    alphaVacc,sigmaVacc,N = par.alphaVacc ,par.sigmaVacc ,par.N 
   

    #create array
    H0 = H0_par
    D0 = D0_par
    I0 = I0_par
    S0 = (N - H0 - D0 - I0) * S0_par
    R0 = N - S0 - I0 - H0 - D0

    F10, F20, F30 = 0, 0, 0
    W0, V0, rema0 = 0, 0, 0
    
    beta0 = betaL + (betaH-betaL) * (1+tanh(K*(thHH-H0)))/2 

    return np.array([S0, I0, R0, H0, D0, beta0, F10, F20, F30, W0, V0, rema0])


@lru_cache(maxsize=32)
def getX0At2712(par):
    '''
    Get initial condition at the week of 21--28 December (week number 16).
    '''
       
    model = getModel(par, Dw="2w")
    simulator = getSimulator(model, par)
    simulator.x0 = getX0(par)
    
    for k in range(16):
        y_next = simulator.make_step(np.zeros((2,1)))
    
    data2712 = getData(simulator)[0].iloc[-1]
    F10, F20, F30, W0, V0, rema0 = 0, 0, 0, 0, 0, 0
    
    return np.array([data2712.S, data2712.I, data2712.R, data2712.H, data2712.D, data2712.beta, F10, F20, F30, W0, V0, rema0])



#############################
# COMPUTE COST
#############################
def computeCostT(data, par):    
    '''
    Compute the cost function on data
    Return normalized cost at each timestep associate with H, beta and the total cost
    '''
    
    dataX, dataTvp, dataU = data
    
    costHt = (dataX.H / par.WH)**2 
    costBetat = ((par.betaH - dataX.beta)/par.WBeta)**2
    costTotT = costHt + costBetat
    
    return costHt, costBetat ,costTotT



def computeCost(data, par):    
    '''
    Compute the cost function on data
    Return normalized cost associate with H, beta and the total cost
    '''
    
    costHt, costBetat, costTotT =  computeCostT(data, par)
    return sum(costHt), sum(costBetat), sum(costTotT)





#############################
# EXTRA
#############################
def getData(simulator):
    '''
    transform simulator to pandas dataframe with time evolution of the system
    return: time series state, time series Tvp (null), time series controll
    '''
    
    dataX = simulator.data["_x"]
    dataTvp = simulator.data["_tvp"]
    dataU = simulator.data["_u"]

    dataX = pd.DataFrame(dataX, columns=["S", "I", "R", "H", "D", "beta", "F1", "F2", "F3", "W", "V", "rema"])
    dataTvp = pd.DataFrame()
    dataU = pd.DataFrame(dataU, columns=["first dose", "second dose"])
    
    return dataX.iloc[:], dataTvp.iloc[:], dataU.iloc[:]
    return dataX, dataTvp, dataU